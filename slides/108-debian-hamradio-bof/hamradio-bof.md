---
title:
- Debian Hamradio BoF
author:
- Debian Hamradio Maintainers
theme:
- Copenhagen
date:
- July 23, 2022
aspectratio:
- 169
---

# Debian Hamradio Maintainers

![](iu.png)

* debian-hams@lists.debian.org
* #debian-hams on OFTC
* https://salsa.debian.org/debian-hamradio-team
* https://qa.debian.org/developer.php?login=debian-hams@lists.debian.org

# People

* tony mancill KG7IEL -- digital modes, trustedqsl, everything
* Christoph Berg DF7CB -- digital modes, SDR, TRX control, everything
* A. Maitland Bottoms AA4HS -- GNU Radio, SDR, digital voice
* Andreas Bombe DD8AB -- SDR, libraries
* Dave Hibberd MM0RFN -- AX.25, APRS, chirp
* Daniele Forsi IU5HKX -- everything
* Ana Custura -- AX.25, utilities
* Francois Marier VA7GPL -- AX.25
* Ervin Hegedüs HA2OS -- tlf
* Göran Weinholt -- multimon-ng
* Jaime Robles EA4TV -- klog
* Ondřej Nový -- tucnak
* Taowa, Federico Grau KC3MWD -- pat
* Gürkan Myczko -- welle.io
* Carles Fernandez -- gnss-sdr

# Contact!

```
2021-04-05 15:50 DF7CB HA2OS 7.0329 CW
2022-03-14 17:18 DF7CB HA2OS 3.5461 CW
```

# New Packages

# hamradio-files -- "Country Files"

Used by wsjtx, jtdx, tlf, xdx, xlog

\scriptsize
```
Package: hamradio-files
Version: 20220630
Installed-Size: 910
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Architecture: all
Description-en: Ham radio call sign and prefix lists
 Ham radio call signs start with prefixes that indicate which country issued
 their license. The country files are listings of these prefixes together with
 exceptions for use in logging software. The Super Check Partial database files
 provide a list of call signs used by active contesters.
 .
 Contained files:
  * cty.csv, cty.dat ("bigcty" from country-files.com)
  * MASTER.SCP (from supercheckpartial.com)
  * WAG_call_history.txt (from www.dk9tn.de/downloads.php)
Homepage: https://www.country-files.com
Section: hamradio
```

# pat -- winlink front-end

![](pat.png)

# wfview -- ICOM transceivers front-end

![](wfview.png)

# kappanhang -- ICOM transceivers textish front-end

![](kappanhang.png)

# JTDX -- Alternative to wsjtx for FT8 digital modes

![](JTDX.png)

# Future Package: SDRangel ?

Huge number of features (and huge number of minified JS files)

![](sdrangel.png)

# Tinkering: CW Paddles via MIDI and PulseAudio

* Debian Arduino package
* New python3-pulsectl package

![](digisparkkeyer.jpg)

https://github.com/df7cb/df7cb-shack/tree/master/midicwkeyer

